/* eslint-env node */

const path = require("path"),
  { resolve } = path;
import { defineConfig } from "vite";
import elmPlugin from "vite-plugin-elm";

module.exports = defineConfig({
  build: {
    rollupOptions: {
      input: Object.assign({
        main: resolve(__dirname, "index.html"),
      }),
    },
  },

  plugins: [elmPlugin()],
});
