{
  description = "Using Vite with Elm";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils = {
    url = "github:numtide/flake-utils";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      in rec {
        packages = flake-utils.lib.flattenTree { hello = pkgs.hello; };
        defaultPackage = packages.hello;
        apps.hello = flake-utils.lib.mkApp { drv = packages.hello; };
        defaultApp = apps.hello;
        devShell = pkgs.mkShell rec {
          packages = with pkgs; [
            elmPackages.elm
            elmPackages.elm-format
            elmPackages.elm-language-server
            html-tidy
            nodejs
            yarn
          ];
        };
      });
}
