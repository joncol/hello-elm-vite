module Main exposing (main, view)

import Browser
import Html exposing (Html, div, h1, img, p, text)
import Html.Attributes exposing (class)


main : Program () () msg
main =
    Browser.sandbox { init = (), update = update, view = view }


update : msg -> model -> model
update _ model =
    model


view : a -> Html msg
view _ =
    div [ class "font-josefin-sans" ]
        [ div [ class "w-[375px]" ]
            [ img [ class "logo-image" ] []
            , img [ class "hero-image" ] []
            , h1 [ class "text-2xl" ] [ text "We're coming soon" ]
            , p []
                [ text
                    """Hello fellow shoppers! We're currently building our new
    fashion store. Add your email below to stay up-to-date with
    announcements and our launch deals."""
                ]
            , div [ class "flex flex-row items-center justify-between w-full" ]
                [ h1 [] [ text "Email Address" ]
                , div
                    [ class "px-6 py-2 w-max bg-red-200 rounded-full" ]
                    [ img [ class "arrow-icon object-contain" ] [] ]
                ]
            ]
        ]
